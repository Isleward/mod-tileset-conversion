To use this mod, copy the source into a new folder in your isleward/server/mods/
folder. The name of the folder doesn't matter.

In index.js, there are two variables you can modify:

```
spriteSize: How big a sprite is in the spritesheet (default is 8)
scale: How big a tile should render in-game (default is 40)
```

Next, you need to make sure that all the sprite files are present. For the mod
to work, all of these need to be there:

```
charas
tiles
walls
mobs
bosses
bigObjects
objects
characters
attacks
ui
abilityIcons
uiIcons
items
materials
questItems
auras
sprites
animChar
animMob
animBoss
```

IF you don't want to modify all the sprites, you can just copy the base ones in
from isleward/client/images/ and modify the ones you DO want to alter.

Currently, the theoretical max size for a sprite is 136px.