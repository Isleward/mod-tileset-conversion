define([
	
], function(
	
) {
	return {
		name: 'Tileset Conversion',

		resourceInfo: {
			spriteSize: 16,
			scale: 32
		},

		init: function() {
			this.events.on('onBeforeGetResourceList', this.beforeGetResourceList.bind(this));
			this.events.on('onBeforeGetResourceInfo', this.beforeGetResourceInfo.bind(this));
		},

		beforeGetResourceList: function(list) {
			list.forEach(function(l) {
				list.push(`${l}|${this.folderName}/images/${l}.png`);
			}, this);

			list.splice(0, list.length / 2);

			console.log(list);
		},

		beforeGetResourceInfo: function(info) {
			extend(true, info, this.resourceInfo);
		}
	};
});